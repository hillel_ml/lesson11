from sklearn.ensemble import VotingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier

from sklearn import preprocessing
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

# Max Voting
print('---- Max Voting ----')
X, y = load_iris(return_X_y=True)
X_scale = preprocessing.scale(X)
X_train, X_test, y_train, y_test = train_test_split(X_scale, y, test_size=0.33, random_state=42)

model_1 = LogisticRegression(random_state=1)
model_2 = DecisionTreeClassifier(random_state=1)
model_3 = KNeighborsClassifier()

model = VotingClassifier(estimators=[('lr', model_1),
                                     ('dt', model_2)],
                         voting='hard')

model.fit(X_train, y_train)
print(model.score(X_test, y_test))

# Averaging
print('---- Averaging ----')
model_1 = LogisticRegression()
model_2 = DecisionTreeClassifier()
model_3 = KNeighborsClassifier()

model_1.fit(X_train, y_train)
model_2.fit(X_train, y_train)
model_3.fit(X_train, y_train)

pred_1 = model_1.predict_proba(X_test)
pred_2 = model_2.predict_proba(X_test)
pred_3 = model_3.predict_proba(X_test)

final_pred = (pred_1 + pred_2 + pred_3) / 3
print(final_pred)


# Weighted Averaging
print('---- Weighted Averaging ----')

w_1 = 0.3
w_2 = 0.3
w_3 = 0.4

final_pred = (pred_1 * w_1 + pred_2 * w_2 + pred_3 * w_3) / 3
print(final_pred)







