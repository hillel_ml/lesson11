# # Stacking
import numpy as np
import pandas as pd


# ### Importing Dataset
data = pd.read_csv('lesson11/data_cleaned.csv')

#printing the first few rows
print(data.head())

#size of the data
print(data.shape)

#checking missing values
print(data.isnull().sum())

# ### Separating Dependent and Independent Variables
#independent variables
x = data.drop(["Survived"], axis = 1)
print(x.shape)

#dependent variables
y = data['Survived']
print(y.shape)

# ### Making test and training set


from sklearn.model_selection import train_test_split

train_x, test_x, train_y, test_y = train_test_split(x, y, random_state = 9 , stratify = y)
print(train_x.shape, test_x.shape, train_y.shape, test_y.shape)

# ## Base models 

#importing predictive models
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier

# ### Model Training and Predictions 

def model_predictions(model, train_x, train_y, test_x):
    
    #train the model
    model.fit(train_x,train_y)
    
    #storing predictions for train and test
    pred_train=model.predict(train_x)
    pred_test=model.predict(test_x)
    return pred_train, pred_test



#Model 1 - Decision Tree
DT = DecisionTreeClassifier(random_state= 101)
M1_train, M1_test = model_predictions(DT, train_x, train_y, test_x)


#Feature Scaling
from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
scaler.fit(X=train_x)

train_x = scaler.transform(train_x)
test_x = scaler.transform(test_x)

#Model 2 - Logistic Regression
LR = LogisticRegression(random_state= 101)
M2_train, M2_test = model_predictions(LR, train_x, train_y, test_x)

#Model 3 - k Nearest Neighbour
knn = KNeighborsClassifier()
M3_train, M3_test = model_predictions(knn, train_x, train_y, test_x)


# ## Stacking Model

print('Creating a New train dataframe')
train_prediction = {
              'DT': M1_train,
              'LR': M2_train,
              'knn': M3_train
              }
train_predictions = pd.DataFrame(train_prediction)
print(train_predictions.head())

print('Creating a New test dataframe')
test_prediction = {
              'DT': M1_test,
              'LR': M2_test,
              'knn': M3_test
              }
test_predictions = pd.DataFrame(test_prediction)
print(test_predictions.head())


# Stacker Model
model = LogisticRegression()
model.fit(train_predictions, train_y)
print(model.score(test_predictions, test_y))



