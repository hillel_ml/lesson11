from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import fbeta_score
from sklearn import model_selection

from lazypredict.Supervised import LazyClassifier

import lightgbm as lgb
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


stars = pd.read_csv('lesson11/Stars.csv')

print(stars.head())

# ====== Encoding ======

# Removing NaN values
stars = stars.dropna()

# Printing the target data column
print("Printing the target data column:")
print(stars.Type)

#printing the first few rows
print(stars.head())

#size of the data
print(stars.shape)

print(stars.info())

print(stars.describe())

print(stars.describe(include='object'))

#checking missing values
print(stars.isnull().sum())

stars = pd.get_dummies(data=stars,columns=["Color","Spectral_Class"],drop_first=True)

print(stars.head().transpose())

# Separating Dependent and Independent Variables
#independent variables
x_data = stars.drop('Type', axis=1)
print(x_data.shape)

#dependent variables
y_data = stars['Type']
print(y_data.shape)

# Making test and training set
x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.25, random_state=0)

clf = LazyClassifier(verbose=0, ignore_warnings=True, custom_metric=None)
models, predictions = clf.fit(x_train, x_test, y_train, y_test)

print(models)

lgbm_clf = lgb.LGBMClassifier()
lgbm_clf.fit(x_train, y_train)

y_pred=lgbm_clf.predict(x_test)

# print(y_pred)

print('Printing Metrics')

accuracy = accuracy_score(y_pred, y_test)

print('LightGBM Model accuracy score: {0:0.4f}'.format(accuracy_score(y_test, y_pred)))

y_pred_train = lgbm_clf.predict(x_train)

print('Training-set accuracy score: {0:0.4f}'. format(accuracy_score(y_train, y_pred_train)))

# print the scores on training and test set

print('Training set score: {:.4f}'.format(lgbm_clf.score(x_train, y_train)))

print('Test set score: {:.4f}'.format(lgbm_clf.score(x_test, y_test)))

print('Printing F1 scores')

score_f1_macro = f1_score(y_test, y_pred, average='macro')
print("Score F1 Macro: ", score_f1_macro)

score_f1_micro = f1_score(y_test, y_pred, average='micro')
print("Score F1 Micro: ", score_f1_micro)

score_f1_weighted = f1_score(y_test, y_pred, average='weighted')
print("Score F1 Weighted: ", score_f1_weighted)

score_f1_none = f1_score(y_test, y_pred, average=None)
print("Score F1 None: ", score_f1_none)

print('Printing F1 beta scores')

score_f1_macro_beta = fbeta_score(y_test, y_pred, average='macro', beta = 0.5)
print("Score F1 Macro Beta: ", score_f1_macro_beta)

score_f1_micro_beta = fbeta_score(y_test, y_pred, average='micro', beta = 0.5)
print("Score F1 Micro Beta: ", score_f1_micro_beta)

score_f1_weighted_beta = fbeta_score(y_test, y_pred, average='weighted', beta = 0.5)
print("Score F1 Weighted Beta: ", score_f1_weighted_beta)

score_f1_none_beta = fbeta_score(y_test, y_pred, average=None, beta = 0.5)
print("Score F1 None Beta: ", score_f1_none_beta)


kfold = model_selection.KFold(n_splits=10, random_state=7, shuffle=True)
scoring = 'accuracy'
results_acc = model_selection.cross_val_score(lgbm_clf, x_test, y_test, cv=kfold, scoring=scoring)
print("Accuracy: %.3f (%.3f)" % (results_acc.mean(), results_acc.std()))



scoring = 'neg_log_loss'
results_log = model_selection.cross_val_score(lgbm_clf, x_test, y_test, cv=kfold, scoring=scoring)
print("Logloss: %.3f (%.3f)" % (results_log.mean(), results_log.std()))


scoring = 'roc_auc'
results_roc = model_selection.cross_val_score(lgbm_clf, x_test, y_test, cv=kfold, scoring=scoring)
print("AUC: %.3f (%.3f)" % (results_roc.mean(), results_roc.std()))

scoring = 'neg_mean_absolute_error'
results_mae = model_selection.cross_val_score(lgbm_clf, x_test, y_test, cv=kfold, scoring=scoring)
print("MAE: %.3f (%.3f)" % (results_mae.mean(), results_mae.std()))


scoring = 'neg_mean_squared_error'
results_mse = model_selection.cross_val_score(lgbm_clf, x_test, y_test, cv=kfold, scoring=scoring)
print("MSE: %.3f (%.3f)" % (results_mse.mean(), results_mse.std()))


scoring = 'r2'
results_r2 = model_selection.cross_val_score(lgbm_clf, x_test, y_test, cv=kfold, scoring=scoring)
print("R^2: %.3f (%.3f)" % (results_r2.mean(), results_r2.std()))


cm = confusion_matrix(y_test, y_pred)
print('Confusion matrix\n\n', cm)
print('\nTrue Positives(TP) = ', cm[0,0])
print('\nTrue Negatives(TN) = ', cm[1,1])
print('\nFalse Positives(FP) = ', cm[0,1])
print('\nFalse Negatives(FN) = ', cm[1,0])

# print(cm)


# Visualize confusion matrix
plt.imshow(cm, interpolation='nearest', cmap="YlGnBu")
plt.title('Confusion matrix')
plt.colorbar()
ticks = np.arange(5)
plt.xticks(ticks, ticks)
plt.yticks(ticks, ticks)
plt.ylabel('True labels')
plt.xlabel('Predicted labels')
plt.show()



plot_confusion_matrix(lgbm_clf, x_test, y_test) 
plt.show()